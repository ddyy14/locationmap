package edu.sjsu.android.googleandsqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LocationsDB extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "googleMap";
    private static int DATABASE_VERSION = 1;
    private static final String DATABASE_TABLE = "maplocations";
    public static final String ID = "id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ZOOMLEVEL = "zoom";

    private SQLiteDatabase mDB;


    public LocationsDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mDB = getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + DATABASE_TABLE + " ( " +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                LONGITUDE + " double , " +
                LATITUDE + " double , " +
                ZOOMLEVEL + " text " +
                " ) ";

        db.execSQL(sql);
    }

    public long insert(ContentValues contentValues){
        long rowID = mDB.insert(DATABASE_TABLE, null, contentValues);
        return rowID;
    }

    public int delete(){
        int cnt = mDB.delete(DATABASE_TABLE, null, null);
        return cnt;
    }

    public Cursor getAllLocations(){
        return mDB.query(DATABASE_TABLE, new String[] {ID, LATITUDE, LONGITUDE, ZOOMLEVEL}, null, null, null, null, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion != newVersion){
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }
}
